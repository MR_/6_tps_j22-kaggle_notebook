# Tabular Playground Series - Jan 2022 

This is a Notebook for the Kaggle competition, Tabular Playground Series - Jan 2022: 

https://www.kaggle.com/competitions/tabular-playground-series-jan-2022

## Goal

The goal of notebook is to predict 1 year of sales for 3 different items at 2 different stores in 3 different countries using the last 4 years. 

## Evaluation

The evalutation is based on SMAPE:

$`\frac{100\%}{n}\displaystyle\sum_{t=1}^{n} \frac{|Ft-At|}{(|At|+|Ft|)/2}`$

## Main Contents

- Generalized additive model

## Kaggle's notebook:

https://www.kaggle.com/code/mr0024/tps-j22-using-gam

## Requirements

The requirements file contains all the packages needed to work through this notebook. In addition a R environment is needed.
